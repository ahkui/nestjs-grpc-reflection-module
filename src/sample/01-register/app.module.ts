import { Module } from '@nestjs/common';
import { GrpcReflectionModule } from '../../grpc-reflection';
import { grpcClientOptions } from './grpc-client.options';
import { HelloModule } from '../common/hello.module';

@Module({
  imports: [HelloModule, GrpcReflectionModule.register(grpcClientOptions)],
})
export class AppModule {}
