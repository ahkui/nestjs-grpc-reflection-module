import {
  FileDescriptorProto,
  FileDescriptorSet,
  DescriptorProto,
  EnumDescriptorProto,
  FieldDescriptorProto,
  OneofDescriptorProto,
  ServiceDescriptorProto,
} from 'google-protobuf/google/protobuf/descriptor_pb';

import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';

import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import {
  FileDescriptorResponse,
  ListServiceResponse,
} from './proto/grpc/reflection/v1alpha/reflection';

export class ReflectionError extends Error {
  constructor(readonly statusCode: grpc.status, readonly message: string) {
    super(message);
  }
}

@Injectable()
export class GrpcReflectionService implements OnModuleInit {
  private readonly logger = new Logger(GrpcReflectionService.name);

  /** The full list of proto files (including imported deps) that the gRPC server includes */
  private fileDescriptorSet = new FileDescriptorSet();

  /** An index of proto files by file name (eg. 'sample.proto') */
  private fileNameIndex: Record<string, FileDescriptorProto> = {};

  /** An index of fully qualified symbol names (eg. 'sample.Message') to the files that contain them */
  private symbolMap: Record<string, FileDescriptorProto> = {};

  constructor(
    @Inject(GRPC_CONFIG_PROVIDER_TOKEN)
    private readonly grpcConfig: GrpcOptions,
  ) {}

  async onModuleInit() {
    const { protoPath, loader } = this.grpcConfig.options;
    const protoFiles = Array.isArray(protoPath) ? protoPath : [protoPath];
    const packageDefinitions = await Promise.all(
      protoFiles.map((file) => protoLoader.load(file, loader)),
    );

    packageDefinitions.forEach((packageDefinition) => {
      Object.values(packageDefinition).forEach(({ fileDescriptorProtos }) => {
        // Add file descriptors to the FileDescriptorSet.
        // We use the Array check here because a ServiceDefinition could have a method named the same thing
        if (Array.isArray(fileDescriptorProtos)) {
          fileDescriptorProtos.forEach((bin) => {
            const proto = FileDescriptorProto.deserializeBinary(bin);
            const isFileInSet = this.fileDescriptorSet
              .getFileList()
              .map((f) => f.getName())
              .includes(proto.getName());
            if (!isFileInSet) {
              this.fileDescriptorSet.addFile(proto);
            }
          });
        }
      });
    });

    this.fileNameIndex = Object.fromEntries(
      this.fileDescriptorSet.getFileList().map((f) => [f.getName(), f]),
    );

    const fileByPackageName = Object.fromEntries(
      this.fileDescriptorSet.getFileList().map((f) => [f.getPackage(), f]),
    );

    const processField = (
      prefix: string,
      file: FileDescriptorProto,
      field: FieldDescriptorProto,
    ) => {
      this.symbolMap[`${prefix}.${field.getName()}`] = file;

      // if referencing field from other package, add that package as a dependency
      const pkg = field.getTypeName().split('.').slice(0, -1).join('.');
      if (pkg && pkg != file.getPackage()) {
        const referencedFile = fileByPackageName[pkg];
        file.addDependency(referencedFile.getName());
      }
    };

    const processOneOf = (
      prefix: string,
      file: FileDescriptorProto,
      decl: OneofDescriptorProto,
    ) => {
      this.symbolMap[`${prefix}.${decl.getName()}`] = file;
    };

    const processMessage = (
      prefix: string,
      file: FileDescriptorProto,
      type: DescriptorProto,
    ) => {
      const name = `${prefix}.${type.getName()}`;
      this.symbolMap[name] = file;

      type
        .getNestedTypeList()
        .forEach((type) => processMessage(name, file, type));
      type.getEnumTypeList().forEach((type) => processEnum(name, file, type));
      type.getFieldList().forEach((field) => processField(name, file, field));
      type.getOneofDeclList().forEach((decl) => processOneOf(name, file, decl));
    };

    const processEnum = (
      prefix: string,
      file: FileDescriptorProto,
      type: EnumDescriptorProto,
    ) => {
      const name = type.getName();
      type.getValueList().forEach((value) => {
        const valueName = value.getName();
        this.symbolMap[`${prefix}.${name}.${valueName}`] = file;
      });
    };

    const processService = (
      prefix: string,
      file: FileDescriptorProto,
      service: ServiceDescriptorProto,
    ) => {
      const name = `${prefix}.${service.getName()}`;
      this.symbolMap[name] = file;
      service.getMethodList().forEach((method) => {
        const methodName = method.getName();
        this.symbolMap[`${name}.${methodName}`] = file;
      });
    };

    this.fileDescriptorSet.getFileList().forEach((file) => {
      const packageName = file.getPackage();

      file
        .getEnumTypeList()
        .forEach((type) => processEnum(packageName, file, type));
      file
        .getMessageTypeList()
        .forEach((type) => processMessage(packageName, file, type));
      file
        .getServiceList()
        .forEach((service) => processService(packageName, file, service));
    });
  }

  /** List the full names of registered gRPC services
   *
   * note: the spec is unclear as to what the 'listServices' param can be; most
   * clients seem to only pass '*' but unsure if this should behave like a
   * filter. Until we know how this should behave with different inputs this
   * just always returns *all* services.
   *
   * @returns full-qualified service names (eg. 'sample.SampleService')
   */
  listServices(listServices: string): ListServiceResponse {
    this.logger.debug(`listServices called with filter ${listServices}`);
    const services = this.fileDescriptorSet
      .getFileList()
      .map((file) =>
        file
          .getServiceList()
          .map((service) => `${file.getPackage()}.${service.getName()}`),
      )
      .flat();

    this.logger.debug(`listServices found services: ${services.join(', ')}`);
    return { service: services.map((service) => ({ name: service })) };
  }

  /** Find the proto file(s) that declares the given fully-qualified symbol name
   *
   * @param symbol fully-qualified name of the symbol to lookup
   * (e.g. package.service[.method] or package.type)
   *
   * @returns descriptors of the file which contains this symbol and its imports
   */
  fileContainingSymbol(symbol: string): FileDescriptorResponse {
    this.logger.debug(`fileContainingSymbol called for symbol ${symbol}`);
    const file = this.symbolMap[symbol];

    if (!file) {
      this.logger.error(`fileContainingSymbol failed to find symbol ${symbol}`);
      throw new ReflectionError(
        grpc.status.NOT_FOUND,
        `Symbol not found: ${symbol}`,
      );
    }

    const deps = file.getDependencyList().map((dep) => this.fileNameIndex[dep]);
    this.logger.debug(
      `fileContainingSymbol found files: ${[file, ...deps].map((f) =>
        f.getName(),
      )}`,
    );

    return {
      fileDescriptorProto: [file, ...deps].map((proto) =>
        proto.serializeBinary(),
      ),
    };
  }

  /** Find a proto file by the file name
   *
   * @returns descriptors of the file which contains this symbol and its imports
   */
  fileByFilename(filename: string): FileDescriptorResponse {
    this.logger.debug(`fileByFilename called with filename ${filename}`);
    const file = this.fileNameIndex[filename];

    if (!file) {
      throw new ReflectionError(
        grpc.status.NOT_FOUND,
        `Proto file not found: ${filename}`,
      );
    }

    const deps = file.getDependencyList().map((dep) => this.fileNameIndex[dep]);
    this.logger.debug(
      `fileContainingSymbol found files: ${[file, ...deps].map((f) =>
        f.getName(),
      )}`,
    );

    return {
      fileDescriptorProto: [file, ...deps].map((f) => f.serializeBinary()),
    };
  }
}
