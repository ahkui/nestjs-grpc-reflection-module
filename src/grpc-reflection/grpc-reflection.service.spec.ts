import { Test } from '@nestjs/testing';
import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import { GrpcReflectionService } from './grpc-reflection.service';

// use grpc options from sample service for testing
import { grpcClientOptions } from '../sample/01-register/grpc-client.options';
import { FileDescriptorProto } from 'google-protobuf/google/protobuf/descriptor_pb';

describe('GrpcReflectionService', () => {
  let reflectionService: GrpcReflectionService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        GrpcReflectionService,
        {
          provide: GRPC_CONFIG_PROVIDER_TOKEN,
          useValue: grpcClientOptions,
        },
      ],
    }).compile();

    await moduleRef.init();

    reflectionService = await moduleRef.resolve<GrpcReflectionService>(
      GrpcReflectionService,
    );
  });

  describe('listServices()', () => {
    it('lists all services', () => {
      const { service: services } = reflectionService.listServices('*');
      expect(services).toHaveLength(2);
      expect(services[0].name).toEqual('sample.SampleService');
    });
  });

  describe('fileByFilename()', () => {
    it('finds files with transitive dependencies', () => {
      const descriptors = reflectionService
        .fileByFilename('sample.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(2);
      expect(descriptors[0].getName()).toEqual('sample.proto');
      expect(descriptors[1].getName()).toEqual('vendor.proto');
    });

    it('finds files with no transitive dependencies', () => {
      const descriptors = reflectionService
        .fileByFilename('vendor.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(1);
      expect(descriptors[0].getName()).toEqual('vendor.proto');
    });

    it('errors with no file found', () => {
      expect(() =>
        reflectionService.fileByFilename('nonexistent.proto'),
      ).toThrow('Proto file not found');
    });
  });

  describe('fileContainingSymbol()', () => {
    it('finds symbols and returns transitive file dependencies', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('sample.HelloRequest')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(2);
      expect(descriptors[0].getName()).toEqual('sample.proto');
      expect(descriptors[1].getName()).toEqual('vendor.proto');
    });

    it('finds imported message types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('vendor.CommonMessage')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(1);
      expect(descriptors[0].getName()).toEqual('vendor.proto');
    });

    it('finds nested message types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('sample.HelloRequest.HelloNested')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(2);
      expect(descriptors[0].getName()).toEqual('sample.proto');
    });

    it('errors with no symbol found', () => {
      expect(() =>
        reflectionService.fileContainingSymbol('non.existant.symbol'),
      ).toThrow('Symbol not found:');
    });
  });
});
